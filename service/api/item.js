var request = require('request');

// comment
var insertWithHttp= function(item,cb){
	request.post(
	    'https://dalager01.azure-mobile.net/tables/Item',
	    { 
			headers: {
				"Accept": "application/json",
				"X-ZUMO-APPLICATION": "MUOeLGKCZYdIAroaarRJOxWbAHkLwe91",
				"Content-Type": "application/json",
				"Host": "dalager01.azure-mobile.net",
				"Content-Length": "56"			    
	         },
	         json:{"name":item,owner:'Twitter:3546381'}
	     },
	    function (error, resp, body) {
	        if(error){
                console.log(error);
	        }
	        if (!error && resp.statusCode == 200) {
	            console.log(body);
	        }
	       cb();
	    }
	);
};
var insertWithApi = function (item,request,cb){
	var tables = request.service.tables;
	var itemTable = tables.getTable('Item');
	itemTable.insert({name:item,owner:'Twitter:3546381'},{
		success:function(res){
			console.log('insert ok:');
			console.log(res);
			cb('inserted with api. No triggering');
		},
		error:function(err){
			console.error('failed:'+JSON.stringify(err));
			cb('Error:'+ JSON.stringify(err));
		}
	});
};

exports.get = function(request, response) {
	var item = request.query.item || 'Standard item';

	 insertWithHttp(item,function(){
	 	response.send('inserted with HTTP, expect triggering');
	 });

	//insertWithApi(item,request,function(txt){
	//	response.send(txt);
	//});
};