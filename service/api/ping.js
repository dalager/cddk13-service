var fs = require('fs');
var _ = require('underscore');
exports.post = function (request, response) {
    // Use "request.service" to access features of your mobile service, e.g.:
    //   var tables = request.service.tables;
    //   var push = request.service.push;

    response.send(statusCodes.OK, { message : 'Hello World!' });
};

//var npmls = function(cb) {
//    require('child_process').exec('npm ls --json --global', function (err, stdout, stderr) {
//        if (err) return cb(err)
//        cb(null, JSON.parse(stdout));
//    });
//}


exports.get = function(request, response) {
    console.log(request.query.path);
    var path = '.';
    if (request.query.path) {
        path = request.query.path;
    }
    
    if (request.query.file) {
        fs.readFile(path, function (err, data) {
            response.send(statusCodes.OK, data.toString());
        });
    } else {
        fs.readdir(path, function (e, l) {
            console.log(l);
            response.send(statusCodes.OK, { files: l });
        });
    }
};