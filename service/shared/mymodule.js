﻿exports.upperCaseIt = function(item) {
    item.name = item.name.toUpperCase();
};


var request = require('superagent');
exports.getbeer = function(callback) {
    request.get('http://www.strangebrew.ca/beername.php?Mode=Generate')
        .end(function (res) {
            var matches = res.text.match(/\<b\>([a-z \'\?\-]*)\<\/b\>/i);
            if (matches) {
                callback(matches[1]);
            } else {
                callback(null);
            }
        });
};
